﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace AltostraWebAPI
{
    public class ReposFetcher
    {
        private string gitHost = "api.github.com";

        public async Task<JObject[]> FetchRepositories(string license,
                                  int monthsBack,
                                  string language,
                                  int minStars)
        {
            JObject[] repos = null;

            using (var client = new HttpClient())
            {
                var uri = BuildFetchURL(language, DateTime.Now.AddMonths(-monthsBack), language, minStars);

                HttpResponseMessage response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var responseJsonString = await response.Content.ReadAsStringAsync();
                    repos = JsonConvert.DeserializeObject<JObject[]>(responseJsonString);
                    return repos;
                }
            }

            return repos;
        }

        private Uri BuildFetchURL(string license,
                                  DateTime updatedAt,
                                  string language,
                                  int numberOfStars)
        {
            var q = $"q=license:{license}+stars:>={numberOfStars}+language:" +
                $"{language}+pushed:>={updatedAt.Date:YYYY-MM-DD}+sort:stars-desc";
            UriBuilder builder = new UriBuilder();
            builder.Scheme = "https";
            builder.Host = gitHost;
            builder.Path = $"/repositories/search/{q}";

            return builder.Uri;
        }


    }
}