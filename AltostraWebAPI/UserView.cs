﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace AltostraWebAPI
{
    public class UserView
    {
        private const int TopReposNumber = 5;
        public List<JObject> TopRepositories { get; set; } 
        public List<JObject> OtherRepositories { get; set; }

        public static UserView FromCollection(ICollection<JObject> repos, int topNumber = TopReposNumber)
        {
            return new UserView() { TopRepositories = repos.Take(topNumber).ToList(),
                                    OtherRepositories = repos.Skip(topNumber).ToList()};
        }
    }
}