﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AltostraWebAPI
{
    public class AltostraApiController : ApiController
    {
        // GET api/<controller>
        public async Task<UserView> GetUserView(string license, string language, int minStars)
        {
            var reposFetcher = new ReposFetcher();
            var repos = await reposFetcher.FetchRepositories(license, 6, language, minStars);

            return UserView.FromCollection(repos);
        }
    }
}